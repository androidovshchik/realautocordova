'use strict';
var FILTER_VISIBLE = false;

if (!!window.performance && window.performance.navigation.type == 2) {
    console.log('reload ' + !!window.performance + ' and ' + window.performance.navigation.type);
    window.location.reload();
}

$('.wrapper-main').mCustomScrollbar({
    scrollbarPosition: 'outside',
    setHeight: true,
    scrollInertia: 300,
    callbacks: {
        onTotalScroll: function () {
            loadMore();

        },
        onTotalScrollOffset: 300
    }
});
$('#device_shutdown').click(function () {
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
        if (navigator.app) {
            console.log("navigator.app");
            navigator.app.exitApp();
        } else if (navigator.device) {
            console.log("navigator.device");
            navigator.device.exitApp();
        } else {
            console.log("navigator.none");
        }
    } else {
        console.log("window condition");
        window.close();
    }
});
$('#device_restart').click(function () {
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
        if (navigator.app) {
            console.log("navigator.app");
            navigator.app.loadUrl("file:///android_asset/www/index.html");
        } else if (navigator.device) {
            console.log("navigator.device");
            navigator.device.loadUrl("file:///android_asset/www/index.html");
        } else {
            console.log("navigator.none");
            location.reload();
        }
    } else {
        console.log("location condition");
        location.reload();
    }
});
$('.datepicker-btn').click(function () {
    $(this).addClass('datepicker-btn_active');
});

$('[data-toggle="datepicker"]').datepicker({
    language: 'ru-RU',
    offset: 1,
    format: 'dd',
    autoHide: true
});

$('[data-toggle="datepicker"]').on('pick.datepicker', function (e) {
    e.preventDefault(); // Prevent to pick the date
    var day = $(this).datepicker('getDate', true);
    var month = $(this).datepicker('getMonthName', true);
    var d = new Date($(this).datepicker("getDate"));
    var full_date = d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    if (typeof ORDERS_DATE !== 'undefined') {
        ORDERS_DATE = full_date;
        IN_PROCESS = 0;
        FINISHED = 0;
        current_page = 1;
        pages = 1;
        ITEMS = [];
        loadMore();
    }
    $('.datepicker-btn__date').html(day + ' ' + month.toLowerCase());
    BACK += true;
});

$('[data-toggle="datepicker"]').on('hide.datepicker', function (e) {
    $('.datepicker-btn').removeClass('datepicker-btn_active');
});

// $('.remodal-gallery').on('opening', function () {
//     setTimeout(function () {
//         $('.gallery-slider').owlCarousel({
//             items: 1,
//            nav: true,
//             navSpeed: 600,
//            navText: ['', '']
//        });
//    }, 300);
// });

getCurrentTime('.time');
actionFilter();
quitTimer();
counter();

var BACK = false;

function goBack() {
    if (BACK) {
        console.log('back -2');
        window.history.go(-2);
    } else {
        window.history.go(-1);
    }

}


function getCurrentTime(sel) {
    (function timer() {
        var now = new Date(),
            hours = now.getHours(),
            minutes = now.getMinutes();

        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        $(sel).html(hours + ':' + minutes);

        setTimeout(timer, 1000);
    })();
}


function actionFilter() {
    var state = false;
    $('[data-action="toggle-filter"]').click(function () {
        $('.filter-container').toggleClass('filter-container_is_visible');
        changeFilterBtnStyle();

        if (state) getCheckboxesLength();
        if (FILTER_VISIBLE) {
            FILTER_VISIBLE = false;
        } else {
            FILTER_VISIBLE = true;
        }
        state ? state = false : state = true;
        PART_TYPES_INTS = [];
        PART_TYPES_STRING = [];
        $('.filter__item input:checked').each(function () {
            PART_TYPES_INTS.push(parseInt($(this).attr('name')));
            PART_TYPES_STRING.push($(this).attr('pname'));
        });
        current_page = 1;
        //  $("#span_part_types").html(PART_TYPES_STRING.join(", "));
        TOTAL_ITEMS = [];
        pages = 1;
        FILTER_RESET = true;
        loadMore();
        return false;
    });

    $('[data-action="reset-filter"]').click(function () {
        $('.filter-container')[0].reset();
        $('.filter__item input:checked').each(function () {
            $(this).prop('checked', false);
        });
        PART_TYPES_INTS = [];
        current_page = 1;
        TOTAL_ITEMS = [];
        pages = 1;
        FILTER_RESET = true;
        document.getElementById('apply').click();
        loadMore();
        return false;
    });

    function changeFilterBtnStyle() {
        $('.filter-btn').toggleClass('filter-btn_is_active');
    }

    function getCheckboxesLength() {
        var lng = $('.filter-list [type="checkbox"]:checked').length;

        if (lng) {
            $('.filter-btn').attr('data-checkbox-length', lng);
        } else {
            $('.filter-btn').removeAttr('data-checkbox-length');
        }
    }
}

function sortName(obj) {
    var sortable = [];
    for (var key in obj)
        if (obj.hasOwnProperty(key))
            sortable.push([key, obj[key]]);

    sortable.sort(function (a, b) {
        var x = a[1].toLowerCase(),
            y = b[1].toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });
    return sortable;
}

function quitTimer(sel) {
    var defDistance = 35,
        distance = defDistance,
        interval = void 0;

    $(document).on('opened', '.remodal-quit', function () {
        startTimer();
    }).on('closed', '.remodal-quit', function () {
        resetTimer();
        $('.close-timer').html('' + defDistance);
    });

    var startTimer = function startTimer() {
        interval = setInterval(countdown, 1000);
    };

    var resetTimer = function resetTimer() {
        clearInterval(interval);
        distance = defDistance;
    };

    var countdown = function countdown() {
        $('.close-timer').html(distance--);

        if (distance < 0) {
            $('.remodal-quit').remodal().close();
        }
    };
}

function counter() {
    $('.counter__less').click(function () {
        var $input = $(this).parent().find('input'),
            $count = parseInt($input.val()) - 1,
            $min = parseInt($input.attr('min')) || 0;

        $count = $count < $min ? $min : $count;
        $input.val($count).change();

        return false;
    });

    $('.counter__more').click(function () {
        var $input = $(this).parent().find('input'),
            $count = parseInt($input.val()) + 1,
            $max = $input.attr('max');

        $count = $count < $max ? $count : $max;
        $input.val($count).change();

        return false;
    });
}