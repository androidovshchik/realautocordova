var MODELS = null;
var url_string = window.location.search;
var url = new URLSearchParams(url_string);
var mark_id = parseInt(url.get("mark_id"));
var mark_name = url.get("mark_name");

$("#span_mark").html(mark_name);

function filter(needle) {
    $('div[class=auto-list]').html('');
    $.each(MODELS, function (i, item) {
        if (needle === null || (needle != null && item.name.toLowerCase().indexOf(needle.toLowerCase()) != -1))
            $('div[class=auto-list]').append('<a class="auto-item" href="catalog.html?model_id=' + item.id + '">' + item.name + '</a>')
    });
}

API_REQUEST({method: 'GetModels', auth_key: API_KEY, params: {mark_id: mark_id}}, function success(data) {
    MODELS = data;
    filter(null);
});

$(".searchbox__input").on('input', function (e) {
    filter($('#form_auto_search_input').val());
});
$("#form_auto_search").on("submit", function () {
    filter($('#form_auto_search_input').val());
    return false;
})