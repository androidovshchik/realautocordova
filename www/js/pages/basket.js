var ITEMS;

function modalGalleryClick(id) {
    $.each(Object.keys(ITEMS), function (i, item_id) {
        var item = ITEMS[item_id];
        if (item_id == id) {
            $("#gallery_text").text(item.name);
            $("#gallery_image").css('background-image', 'url(' + item.photo['840x630'] + ')');
        }
    });
}

function cleanBasket() {
    API_REQUEST({
        method: 'CleanBasket',
        auth_key: API_KEY
    }, function success(data) {
        $("#span_all_categories").html('');
        $("#total").html(0);
        setHeaderIcons(data.total, data.summ);
    });
}

function deleteBasket(id) {
    API_REQUEST({
        method: 'DeleteBasket',
        auth_key: API_KEY,
        params: {basket_ids: [id], with_margin: 1}
    }, function success(data) {
        ITEMS = data.basket;
        reload();
        $("#basket" + id).hide();
        $("#total").html(data.summ);
        setHeaderIcons(data.total, data.summ);
    });
    BACK += true;

}

function orderBulk() {
    API_REQUEST({
        method: 'OrderBulk',
        auth_key: API_KEY
    }, function success(data) {
        ITEMS = data.basket;
        reload();
        $("#total").html('0');
        setHeaderIcons(data.total, data.summ);
    });
}

function orderBasket(id) {
    API_REQUEST({
        method: 'Order',
        auth_key: API_KEY,
        params: {basket_ids: [id], with_margin: 1}
    }, function success(data) {
        ITEMS = data.basket;
        reload();
        $("#basket" + id).hide();
        $("#total").html(data.summ);
        setHeaderIcons(data.total, data.summ);
    });
    BACK += true;
}

function editBasket(id, new_quantity) {
    //console.log('Set ' + new_quantity + ' for ' + id)
    API_REQUEST({
        method: 'EditBasket',
        auth_key: API_KEY,
        params: {basket_id: id, qnt: parseInt(new_quantity), with_margin: 1}
    }, function success(data) {
        ITEMS = data.basket;
        reload();
        $("#total").html(data.summ);
        setHeaderIcons(data.total, data.summ);
    });
    BACK += true;
}

function reload() {
    $("#span_all_categories").html('');
    var total = 0;
    if (ITEMS != null) {
        $.each(Object.keys(ITEMS), function (i, item_id) {
            var item = ITEMS[item_id];
            total = total + parseInt(item.price);
            var html = '<div class="catalog-item catalog-item_basket" id="basket' + item_id + '">\n' +
                '                            <div class="catalog-item__image-box" data-remodal-target="gallery" onClick="javascript:modalGalleryClick(\'' + item.id + '\')"><img\n' +
                '                                    class="catalog-item__image" src="' + (item.photo['78x78'] != null ? item.photo['78x78'] : 'img/noimage.png') + '" alt=""></div>\n' +
                '                            <div class="catalog-item__content">\n' +
                '                                <h3 class="catalog-item__title">' + item.good_name + '</h3>\n' +
                '                                <div class="catalog-info catalog-info_basket">\n' +
                '                                    <p class="catalog-info__item">Артикул: <b>' + item.article + '</b></p>\n' +
                '                                    <p class="catalog-info__item">Бренд: <b>' + item.brand_name + '</b></p>\n' +
                '                                    <p class="catalog-info__item">Срок доставки: <b>' + item.min_delivery + ' - ' + item.max_delivery + ' дней</b></p>\n' +
                '                                    <p class="catalog-info__item">Цена за шт: <b class="text_blue">' + item.price + ' ₸</b></p>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="basket-item__counter-wrap">\n' +
                '                                <div class="counter"><a class="counter__less" href="#" onclick="editBasket(' + item.id + ',' + (parseInt(item.qnt) - 1) + ')"></a>\n' +
                '                                    <input class="counter__input" type="text" min="1" max="100" value="' + item.qnt + '"><a\n' +
                '                                            class="counter__more" href="#"  onclick="editBasket(' + item.id + ',' + (parseInt(item.qnt) + 1) + ')"></a>\n' +
                '                                </div>\n' +
                '                                <a class="basket-item__order-btn-link" href="#" onclick="orderBasket(' + item_id + ')">Заказать</a>\n' +
                '                            </div>\n' +
                '                            <div class="catalog-item__actions">\n' +
                '                                <div class="delivery-info">\n' +
                '                                    <time class="delivery-info__date"><b id="add_date">' + formatDate(item.add_date) + '</b></time>\n' +
                '                                    <p class="basket-item__total-text">Итого</p>\n' +
                '                                    <p class="basket-item__total-price">' + (item.price * item.qnt) + ' ₸</p>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="basket-item__remove"><a class="basket-remove" href="#" onclick="deleteBasket(' + item_id + ')"><i\n' +
                '                                    class="icon-remove"></i></a></div>\n' +
                '                        </div>';
            $("#span_all_categories").append(html);
        });
        $("#total").html(total);
    }
}

API_REQUEST({
    method: 'GetBasket',
    auth_key: API_KEY,
    params: {with_margin: 1}
}, function success(data) {
    ITEMS = data.basket;
    reload();
});