var PART_TYPES = null;
var url_string = window.location.search;
var url = new URLSearchParams(url_string);
var model_id = parseInt(url.get("model_id"));
var current_page = 1;
var pages = 1;
var TOTAL_ITEMS = [];

function modalGalleryClick(id) {
    $.each(TOTAL_ITEMS, function (i, item) {
        if (item.id == id) {
            $("#gallery_text").text(item.name);
            $("#gallery_image").css('background-image', 'url(' + item.photo['840x630'] + ')');
        }
    });
}

var current_id = 0;
var is_Edit = false;
$("#to_basket").click(function () {
    if (is_Edit)
        editBasket();
    else sendToBasket();
});

function modalProductClick(id, isEdit) {
    $.each(TOTAL_ITEMS, function (i, item) {
        if (item.id == id) {
            is_Edit = isEdit;
            current_id = id;
            if (is_Edit) {
                var q = parseInt(BASKET_FOR_ITEM[current_id].quantity);
                $("#input_quantity").val(q);
                $("#units").html(q);
                recalc();
            }
            $("#product_item_article_name").html(item.article)
            $("#product_item_name").html(item.name)
            $("#product_item_name_2").html(item.name)
            $("#product_item_brand_name").html(item.brand_name)
            $("#product_image").attr("src", item.photo['78x78']);
            var keys = Object.keys(item.stocks);
            $("#delivery").html('');
            $.each(keys, function (i_stock, stock) {
                var itm = item.stocks[stock];
                if (itm.quantity > 0) {
                    if (i_stock === 0) {
                        setProduct(itm.hash_key, itm.price, itm.quantity);
                    }
                    if (!isEdit) {
                        $("h3[class=remodal-product-parametrs__subtitle]").show();
                        var html = '<label class="delivery-date">\n' +
                            '                                <input onclick="setProduct(\'' + itm.hash_key + '\',' + itm.price + ',' + itm.quantity + ')" class="delivery-date__input" type="radio" name="delivery-date"' + (i_stock == 0 ? ' checked' : '') + '>\n' +
                            '                                <p class="delivery-date__text">' + itm.min_delivery + ' - ' + itm.max_delivery + ' дней</p>\n' +
                            '                                <p class="delivery-date__btn"><span\n' +
                            '                                        class="delivery-date__btn-price">Цена: ' + itm.price + ' ₸</span><span\n' +
                            '                                        class="delivery-date__btn-quantity">Наличие: ' + itm.quantity + ' шт.</span></p>\n' +
                            '                            </label>';
                        $("#delivery").append(html);
                    } else {
                        $("#delivery_info").hide();
                    }
                }
            });

        }
    });
}

function reset() {
    pages = 1;
    current_page = 1;
    PART_TYPES = [];
}

function editBasket() {
    var new_quantity = parseInt($("#input_quantity").val());
    if (new_quantity > 0) {
        API_REQUEST({
            method: 'EditBasket',
            auth_key: API_KEY,
            params: {basket_id: parseInt(BASKET_FOR_ITEM[current_id].basket_id), qnt: new_quantity}
        }, function success(data) {
            setHeaderIcons(data.total, data.summ);
            BASKET_FOR_ITEM[current_id].quantity = new_quantity;
        });
    }
}

function sendToBasket() {
    API_REQUEST({
        method: 'AddToBasket',
        auth_key: API_KEY,
        params: {hash_key: $('#hash').val(), qnt: parseInt($('#input_quantity').val()), with_margin: 1}
    }, function success(data) {
        setHeaderIcons(data.total, data.summ);
        //   console.log('current = ' + current_id)
        ITEMS_IN_BASKET.push(parseInt(current_id));
        filter($('#form_auto_search_input').val(), true);
    });
}

var price_unit = 0;
$("#input_quantity").change(function () {
    $("#units").html($("#input_quantity").val());
    recalc();
});

function recalc() {
    var total_price = parseInt($("#input_quantity").val()) * price_unit;
    $("#price_total").html(total_price);
}

function setProduct(hash, price, max_quantity) {
    $("#quantity_max").html(max_quantity);
    $("#hash").val(hash);
    $("#price_per_unit").html(price);
    price_unit = price;
    $("#input_quantity").attr('max', max_quantity)
    if (!is_Edit) {

        /*if (parseInt($("#input_quantity").val()) > max_quantity) {
            $("#input_quantity").val(max_quantity);
            $("#units").html(max_quantity);
        }*/
        $("#input_quantity").val('1');
        $("#units").html('1');
    }
    recalc();
}

function filter(needle, reset) {
    if (reset)
        $("#span_all_categories").html('');
    if (PART_TYPES !== null) {
        //Object.keys(PART_TYPES).forEach(function (id, i, arr) {
        //});
    }

    $.each(TOTAL_ITEMS, function (i, item) {
        if (needle === null || (needle != null && item.name.toLowerCase().indexOf(needle.toLowerCase()) != -1)) {
            var div_id = 'item_id' + item.id;
            if ($('#' + div_id).length === 0) {
                var available = false;
                var min = 0;
                var max = 0;
                var keys = Object.keys(item.stocks);
                min = item.stocks[keys[0]].price;
                max = item.stocks[keys[0]].price;
                $.each(keys, function (i_stock, stock) {
                    stock = item.stocks[stock];
                    if (parseInt(stock.quantity) > 0) {
                        available = true;
                        if (stock.price > max) max = stock.price;
                        if (stock.price < min || (stock.price > 0 && !(min > 0))) min = stock.price;
                    }
                });
                var row = '<div id="' + div_id + '" class="catalog-item' + (!available ? ' catalog-item_disabled' : '') + '">\n' +
                    '                            <div class="catalog-item__image-box"><a data-remodal-target="gallery" class="modal_gallery" onClick="javascript:modalGalleryClick(\'' + item.id + '\')"><img\n' +
                    '                                    class="catalog-item__image" src="' + (item.photo['78x78'] != null ? item.photo['78x78'] : 'img/noimage.png') + '" alt=""></div>\n' +
                    '                            <div class="catalog-item__content">\n' +
                    '                                <h3 class="catalog-item__title">' + item.name + '</h3>\n' +
                    '                                <div class="catalog-info">\n' +
                    '                                    <p class="catalog-info__item">Артикул: <b>' + item.article + '</b></p>\n' +
                    '                                    <p class="catalog-info__item">Производитель: <b>' + item.brand_name + '</b></p>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                            <div class="catalog-item__actions">\n' +
                    (ITEMS_IN_BASKET.indexOf(parseInt(item.id)) == -1 ?
                        (available ?
                            ('<a class="catalog-item__btn"  onClick="javascript:modalProductClick(\'' + item.id + '\',false)"\n' +
                                '                                                                  data-remodal-target="product"><span\n' +
                                '                                    class="catalog-item__btn-text">Заказать</span><span\n' +
                                '                                    class="catalog-item__btn-price-list">от <span class="catalog-item__btn-price">' + min + ' ₸</span> до <span\n' +
                                '                                    class="catalog-item__btn-price">' + max + ' ₸</span></span></a>\n')
                            : ('<a class="catalog-item__btn" href="#"><span class="catalog-item__btn-text">Нет в наличии</span><span class="catalog-item__btn-price-list">от <span class="catalog-item__btn-price">' + min + ' ₸</span> до <span class="catalog-item__btn-price">' + max + ' ₸</span></span></a>')) : '<div class="catalog-item__product-added">\n' +
                        '                      <p class="catalog-item__product-added-text">Данный товар уже добавлен в корзину.<br> Желаете изменить параметры заказа?</p><a class="catalog-item__change-link" href="#" data-remodal-target="product" onClick="javascript:modalProductClick(\'' + item.id + '\',true)">Изменить</a>\n' +
                        '                    </div>') +
                    '                        </div></div>';
                var part_html = '<span id="part' + item.part_type_id + '" style="display: none;">\n' +
                    '                    <div class="catalog__title-box">\n' +
                    '                        <h2 class="catalog__title">' + item.part_type_name + '</h2>\n' +
                    '                    </div>\n' +
                    '                    <div class="catalog-list" id="part' + item.part_type_id + '_list">\n' +
                    '                    </div>\n' +
                    '                    </span>\n' +
                    '                    ';
                if ($('#part' + item.part_type_id).length == 0) {
                    $("#span_all_categories").append(part_html);
                }

                $('#part' + item.part_type_id).show();
                $('#part' + item.part_type_id + '_list').append(row);
            }
        }
    });
    $.each(PART_TYPES, function (s, filter) {
        if ($('#filter' + filter[0]).length == 0) {
            var filter_html = ' <div class="filter__item" id="filter' + filter[0] + '">\n' +
                '                            <label class="checkbox">\n' +
                '                                <input class="checkbox__input" type="checkbox" pname="' + filter[1] + '" name="' + filter[0] + '"><i class="checkbox__icon"></i>\n' +
                '                                <p class="checkbox__text">' + filter[1] + '</p>\n' +
                '                            </label>\n' +
                '                        </div>';
            $(".filter-list").append(filter_html);
        }
    });
    if (current_page > pages) {
        $("div[class=preloader]").hide();
    }
}

var PART_TYPES_INTS = [];
var PART_TYPES_STRING = [];
var FILTER_RESET = false;

function loadMore() {
    $("#span_part_types").html('Все товары');
    if (current_page <= pages) {
        $("div[class=preloader]").show();
        var params = {model_id: model_id, page: current_page, with_margin: 1};
        if (PART_TYPES_INTS.length > 0) {
            $("#span_part_types").html(PART_TYPES_STRING.join(", "));
            params.part_types = PART_TYPES_INTS;
        }
        if ($('#form_auto_search_input').val().length > 0) {
            params.filter = $('#form_auto_search_input').val();
        }


        API_REQUEST({
            method: 'GetProducts',
            auth_key: API_KEY,
            params: params
        }, function success(data) {
            TOTAL_ITEMS = TOTAL_ITEMS.concat(data.goods);
            PART_TYPES = sortName(data.part_types);
            pages = data.pages;
            $("#product_item_model_name").text(data.model_name)
            $("#span_count").html(data.total);
            $("#span_model").html(data.model_name)
            filter($('#form_auto_search_input').val(), FILTER_RESET);
            if (FILTER_RESET)
                FILTER_RESET = false;
        });
        current_page++;
    }
}

$(".searchbox__input").on('input', function (e) {
    filter($('#form_auto_search_input').val(), true);
});
$("#form_auto_search").on("submit", function () {
    filter($('#form_auto_search_input').val(), true);
    return false;
});

loadMore();

$(document).mouseup(function (e) {
    var container = $(".filter-container.filter-container_is_visible");
    if ((container.has(e.target).length === 0) && (FILTER_VISIBLE)) {
        document.getElementById('apply').click();
    }
});