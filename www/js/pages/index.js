function init() {
    window.location.href = 'auto.html';
}

$("#form_login").on("submit", function () {
    API_REQUEST({
        method: 'TermActivation',
        params: {client_id: parseInt($('#form_login_input').val())}
    }, function success(data) {
        // console.log(JSON.stringify(data));
        if (data.auth_key) {
            var value = JSON.stringify(data.auth_key);
            console.log("store apiKey " + value);
            localStorage.setItem('apiKey', value);
            window.location.href = 'auto.html';
        }
    });
    return false;
});
init();