function getWeather() {
    API_REQUEST({
        method: 'GetWeather'
    }, function success(data) {
        localStorage.removeItem("weather");
        localStorage.removeItem("temp");
        localStorage.setItem('weather', 'url("img/weather/' + data.icon + '.png")');
        localStorage.temp = data.temp;
        $("p[class=weather__text]").html(localStorage.temp);
        $("i[class=icon-weather-cloud-sun]").css('background-image', localStorage.weather);
    });
}

setInterval(getWeather, 900 * 1000); // 600 сек
if (localStorage.temp == null) {
    getWeather();
}

$("p[class=weather__text]").html(localStorage.temp);
$("i[class=icon-weather-cloud-sun]").css('background-image', localStorage.weather);

function setHeaderIcons(count, summ) {
    var html = '<i class="icon-basket header__basket-icon"\n' +
        '                                                                 data-items="' + count + '">\n' +
        '                    <svg width="26" height="28" xmlns="http://www.w3.org/2000/svg">\n' +
        '                        <path class="svg-icon-fill"\n' +
        '                              d="M7.396 18.42h15.388a3.177 3.177 0 0 0 3.168-3.174V8.76v-.023-.046c0-.011 0-.023-.005-.034 0-.012-.006-.029-.006-.04 0-.012-.006-.023-.006-.035-.006-.011-.006-.023-.011-.04-.006-.012-.006-.023-.012-.035-.006-.011-.006-.023-.011-.034a.28.28 0 0 1-.018-.04.126.126 0 0 0-.017-.029l-.017-.035c-.006-.011-.012-.017-.017-.028-.006-.012-.017-.023-.023-.035a.1.1 0 0 0-.023-.029c-.006-.011-.017-.017-.023-.028-.006-.012-.017-.018-.023-.029-.006-.012-.017-.017-.023-.023l-.029-.029c-.011-.006-.017-.017-.028-.023-.012-.006-.023-.017-.035-.023l-.029-.017c-.011-.006-.023-.012-.034-.023l-.034-.017-.035-.018-.034-.017c-.012-.006-.023-.006-.035-.011a.106.106 0 0 0-.04-.012c-.011 0-.023-.006-.029-.006-.017-.005-.028-.005-.046-.005-.005 0-.011-.006-.022-.006L5.784 5.293V2.568c0-.029 0-.057-.006-.08 0-.006 0-.012-.006-.023 0-.018-.006-.035-.006-.052-.005-.017-.005-.029-.011-.046 0-.012-.006-.017-.006-.029l-.017-.052c0-.005-.006-.017-.006-.022a.172.172 0 0 0-.023-.046c-.005-.006-.005-.018-.011-.023-.006-.012-.012-.023-.023-.035-.006-.011-.012-.017-.017-.029l-.018-.028c-.005-.012-.017-.023-.023-.035a27976.561 27976.561 0 0 0-.051-.052L5.543 2c-.012-.011-.023-.023-.04-.034-.006-.006-.018-.012-.024-.018-.011-.011-.023-.017-.034-.028a.53.53 0 0 0-.046-.03c-.006-.005-.011-.005-.017-.01-.023-.012-.052-.024-.075-.035L1.077.06a.776.776 0 1 0-.603 1.432L4.229 3.08V20.087a3.175 3.175 0 0 0 2.76 3.145 3.14 3.14 0 0 0-.447 1.61 3.134 3.134 0 0 0 3.128 3.133 3.138 3.138 0 0 0 3.128-3.133c0-.575-.155-1.121-.43-1.581h6.968a3.094 3.094 0 0 0-.43 1.58 3.134 3.134 0 0 0 3.127 3.134 3.138 3.138 0 0 0 3.128-3.133 3.138 3.138 0 0 0-3.128-3.134H7.396c-.895 0-1.618-.73-1.618-1.621v-2.104a3.2 3.2 0 0 0 1.618.437zm3.857 6.416c0 .874-.711 1.581-1.578 1.581a1.586 1.586 0 0 1-1.578-1.581c0-.868.711-1.581 1.578-1.581.867 0 1.578.707 1.578 1.581zm12.363 0c0 .874-.711 1.581-1.578 1.581a1.586 1.586 0 0 1-1.578-1.581c0-.868.711-1.581 1.578-1.581.867 0 1.578.707 1.578 1.581zm-.832-7.969H7.396c-.895 0-1.618-.73-1.618-1.621V6.857l18.625 2.57v5.813c0 .903-.73 1.627-1.619 1.627z"\n' +
        '                              fill-rule="nonzero"></path>\n' +
        '                    </svg>\n' +
        '                </i><span class="header__basket-price">' + summ + ' ₸</span>';
    $("#header_basket").html(html);
}

var ITEMS_IN_BASKET = [];
var BASKET_FOR_ITEM = {};

API_REQUEST({
    method: 'GetBasket',
    auth_key: API_KEY,
    params: {with_margin: 1}
}, function success(data) {
    setHeaderIcons(data.total, data.summ);
    $.each(Object.keys(data.basket), function (i, item_id) {
        var item = data.basket[item_id];
        ITEMS_IN_BASKET.push(parseInt(item.good_id));
        BASKET_FOR_ITEM[parseInt(item.good_id)] = {basket_id: item_id, quantity: item.qnt};
    });
    //   console.log(ITEMS_IN_BASKET)
});