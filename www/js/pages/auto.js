var CARS = null;

function filter(needle) {
    $('div[class=auto-list]').html('');
    $.each(CARS, function (i, item) {
        if (needle === null || (needle != null && item.name.toLowerCase().indexOf(needle.toLowerCase()) != -1))
            $('div[class=auto-list]').append('<a class="auto-item" href="auto-inner.html?mark_id=' + item.id + '&mark_name=' + encodeURI(item.name) + '">' + item.name + '</a>')
    });
}


API_REQUEST({method: 'GetMarks', auth_key: API_KEY}, function success(data) {
    CARS = data;
    filter(null);
});

$(".searchbox__input").on('input', function (e) {
    filter($('#form_auto_search_input').val());
});
$("#form_auto_search").on("submit", function () {
    filter($('#form_auto_search_input').val());
    return false;
});