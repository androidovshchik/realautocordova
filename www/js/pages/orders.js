var ITEMS = [];
var url_string = window.location.search;
var url = new URLSearchParams(url_string);
var with_margin = 1;
if (url.get("with_margin") != null) {
    with_margin = parseInt(url.get("with_margin"));
}
var IN_PROCESS = 0;
var FINISHED = 0;

function modalGalleryClick(id) {
    $.each(ITEMS, function (i, item) {
        if (item.id == id) {
            $("#gallery_text").text(item.good_name);
            $("#gallery_image").css('background-image', 'url(' + item.photo['840x630'] + ')');
        }
    });
}


function filter(needle, reset) {
    if (reset)
        $("#span_all_categories").html('');
    if (ITEMS != null)
        $.each(Object.keys(ITEMS), function (i, item_id) {
            var item = ITEMS[item_id];
            if (needle === null || (needle != null && item.good_name.toLowerCase().indexOf(needle.toLowerCase()) != -1)) {
                var div_id = 'item_id' + item.id;
                if ($('#' + div_id).length === 0) {
                    var html = '<div class="catalog-item catalog-item_order" id="' + div_id + '">\n' +
                        '                            <div class="catalog-item__image-box" data-remodal-target="gallery" onClick="javascript:modalGalleryClick(\'' + item.id + '\')"><img\n' +
                        '                                    class="catalog-item__image" src="' + (item.photo['78x78'] != null ? item.photo['78x78'] : 'img/noimage.png') + '" alt=""></div>\n' +
                        '                            <div class="catalog-item__content">\n' +
                        '                                <h3 class="catalog-item__title">' + item.good_name + '</h3>\n' +
                        '                                <div class="catalog-info catalog-info_order">\n' +
                        '                                    <p class="catalog-info__item">Артикул: <b>' + item.article + '</b></p>\n' +
                        '                                    <p class="catalog-info__item">Количество: <b>' + item.qnt + ' шт. </b></p>\n' +
                        '                                    <p class="catalog-info__item">Доставлено: <b class="catalog-info__item_blue">' + item.qnt_accept + '\n' +
                        '                                        шт. </b></p>\n' +
                        '                                    <p class="catalog-info__item">Срок доставки: <b>' + item.min_delivery_day + ' - ' + item.max_delivery_day + ' дней</b></p>\n' +
                        '                                    <p class="catalog-info__item">Подтверждено: <b>' + item.qnt_accept + ' шт.</b></p>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="catalog-item__actions">\n' +
                        '                                <div class="delivery-info">\n' +
                        '                                    <time class="delivery-info__date"><b>' + formatDate(item.created_at) + '</b></time>\n' +
                        '                                    <p class="delivery-info__status">Статус: <b>' + item.status.name + '</b></p>\n' +
                        '                                    <p class="delivery-info__price">' + item.price * item.qnt + ' ₸</p>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>';
                    var part_html = '<span id="part' + item.status.group_id + '" style="display: none;">\n' +
                        '                    <div class="catalog__title-box">\n' +
                        '                        <h2 class="catalog__title">' + item.status.group_name + '</h2>\n' +
                        '                    </div>\n' +
                        '                    <div class="catalog-list" id="part' + item.status.group_id + '_list">\n' +
                        '                    </div>\n' +
                        '                    </span>\n' +
                        '                    ';
                    if ($('#part' + item.status.group_id).length == 0) {
                        $("#span_all_categories").append(part_html);
                    }

                    $('#part' + item.status.group_id).show();
                    $('#part' + item.status.group_id + '_list').append(html);
                }
            }
        });

}

var current_page = 1;
var pages = 1;
var ORDERS_DATE = null;
var FILTER_RESET = false;

function loadMore() {
    $("#span_part_types").html('Все товары');
    if (current_page <= pages) {
        $("div[class=preloader]").show();
        var params = {page: current_page, with_margin: with_margin, filter: $('#form_auto_search_input').val()};
        if (ORDERS_DATE != null)
            params.created = ORDERS_DATE;
        FILTER_RESET += true;
        API_REQUEST({
            method: 'GetOrders',
            auth_key: API_KEY,
            params: params
        }, function success(data) {
            if (data.order_items != null) {
                ITEMS = ITEMS.concat(data.order_items);
            }
            filter($('#form_auto_search_input').val(), FILTER_RESET);
            if (FILTER_RESET)
                FILTER_RESET = false;
            current_page++;
            pages = parseInt(data.pages);
        });
    } else {
        $("div[class=preloader]").hide();
    }
}

loadMore();
//$(".searchbox__input").on('input', function (e) {
//   filter($('#form_auto_search_input').val());
//});
//$("#form_auto_search").on("submit", function () {
//    filter($('#form_auto_search_input').val());
///  return false;
//});
$(".searchbox__input").on('input', function (e) {
    filter($('#form_auto_search_input').val(), true);
    current_page = 1;
    pages = 1;
    loadMore();
});
$("#form_auto_search").on("submit", function () {
    current_page = 1;
    pages = 1;
    loadMore();
    return false;
});