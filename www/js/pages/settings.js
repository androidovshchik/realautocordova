API_REQUEST({
    method: 'GetBalance',
    auth_key: API_KEY
}, function success(data) {
    $("#balance").text(data.balance);
});
API_REQUEST({
    method: 'GetProfit',
    auth_key: API_KEY
}, function success(data) {
    $("#profit").text(data.profit);
});
API_REQUEST({
    method: 'GetMargin',
    auth_key: API_KEY
}, function success(data) {
    //  console.log(JSON.stringify(data))
    $("#margin").text(data.margin);
});
$("#form_margin").on("submit", function () {
    API_REQUEST({
        method: 'SetMargin',
        auth_key: API_KEY,
        params: {margin: parseInt($('#form_margin_input').val())}
    }, function success(data) {
        console.log(JSON.stringify(data))
        if (parseInt(data.error) == 0) {
            $("#margin").text($('#form_margin_input').val());
        }
    });
    return false;
});
$("#form_password").on("submit", function () {
    var old_pin = $('#old_pin').val();
    var new_pin = $('#new_pin').val();
    var new_pin_v = $('#new_pin_v').val();
    if (new_pin != new_pin_v) {
        alert('Пароли не совпадают');
        return false;
    }
    API_REQUEST({
        method: 'SetPin',
        auth_key: API_KEY,
        params: {pin: old_pin, new_pin: new_pin}
    }, function success(data) {
        //  console.log(JSON.stringify(data))
        if (data.error == 15) {
            alert('Пароль неверный')
        } else if (data.error == 17) {
            $('.remodal-password').remodal().close();

            alert('Пароль сменен')
        }
    });
    return false;
});