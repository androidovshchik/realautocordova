var ENDPOINT = "http://realauto.ddns.net:3000";
var API_KEY = localStorage.getItem('apiKey');
if (!API_KEY) {
    API_KEY = "62394c26634e4ac848ede56db2c732f6";
}

function formatDate(d) {
    return new Date(d).toLocaleString('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    });
}

function API_REQUEST(data, success) {
    var URL = ENDPOINT + '?data=' + JSON.stringify(data);
    //   console.log(URL)
    $.getJSON(URL)
        .done(success);
}